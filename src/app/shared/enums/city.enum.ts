export enum CityEnum {
  Amsterdam,
  Berlin,
  Milan,
  Rome,
  Paris
}

export const CityCoordinates :any = {
  Amsterdam: {
    latitude: 52.37,
    longitude: 4.90
  },
  Berlin: {
    latitude: 52.51,
    longitude: 13.39
  },
  Milan: {
    latitude: 45.46,
    longitude: 9.18
  },
  Rome: {
    latitude: 41.89,
    longitude: 12.48
  },
  Paris: {
    latitude: 48.83,
    longitude: 2.35
  }
}
