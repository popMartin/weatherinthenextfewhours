import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router'
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import {
  CityListComponent,
  CityThumbnailComponent,
  CityDetailsComponent
} from "./pages/index";
import {
  CityService,
} from "./shared/services/index";
import {
  ICity
} from './shared/models/index'
import { appRoutes } from './router/routes'


@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    CityListComponent,
    CityThumbnailComponent,
    CityDetailsComponent
  ],
  providers: [CityService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
