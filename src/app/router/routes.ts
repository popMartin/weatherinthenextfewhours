import { Routes } from '@angular/router'
import {CityListComponent} from "../pages/city-list/city-list-component/city-list.component";
import {CityDetailsComponent} from "../pages/city-details/city-details-component/city-details.component";


export const appRoutes:Routes = [
  { path: 'cities', component: CityListComponent },
  { path: 'cities/:id', component: CityDetailsComponent },
  { path: '', redirectTo: '/cities', pathMatch: 'full' }
]
