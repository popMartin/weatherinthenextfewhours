import { Location } from "@angular/common";
import { TestBed, fakeAsync, tick } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { Router } from "@angular/router";
import { AppComponent } from '../app.component'
import { CityListComponent } from "../pages/city-list/city-list-component/city-list.component";
import { CityDetailsComponent } from "../pages/city-details/city-details-component/city-details.component";
import { CityService } from "../shared/services/index"
import { appRoutes } from "./routes";

describe("Router: App", () => {
  let location: Location;
  let router: Router;
  let fixture;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(appRoutes),
        HttpClientTestingModule
      ],
      declarations: [CityListComponent, CityDetailsComponent, AppComponent],
      providers : [CityService]
    });

    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(AppComponent);
    router.initialNavigation();
  });

  it('navigate to "" redirects you to /cities', fakeAsync(() => {
    router.navigate(['']);
    tick();
    expect(location.path()).toMatch('/cities');
  }));

  it('navigate to "cities/(id)" redirects you to /cities/(id))', fakeAsync(() => {
    const cityId = 2
    router.navigate([`cities/${cityId}`]);
    tick();
    expect(location.path()).toMatch(`cities/${cityId}`);
  }));
});
