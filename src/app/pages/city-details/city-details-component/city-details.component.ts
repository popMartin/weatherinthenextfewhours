import { Component, OnInit } from '@angular/core'
import { CityService } from 'src/app/shared/services/index'
import { ActivatedRoute } from "@angular/router";
import { formatDate } from '@angular/common';
import { CityEnum } from '../../../shared/enums/index'
import {ICity } from '../../../shared/models/index'
import { catchError } from 'rxjs/operators'

@Component({
  template: `
    <div class="page-wrapper" >
      <div *ngIf="city">
        <div class="row page-header-wrapper">
          <div class="page-header-city-back-btn-wrapper">
            <button [routerLink]="['/cities']" type="button" class="btn page-header-back-btn">
              Back to Dashboard
            </button>
          </div>
          <div class="page-header-city-name-wrapper">
            <span>{{city.name}}</span>
          </div>
        </div>
        <div class="row page-content-wrapper">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 city-info-wrapper">
            <div class="row city-info-date-wrapper">
              <div>{{currentDayOfMonth}}</div>
              <div>{{currentMonth}}</div>
            </div>
            <div class="row city-info-temperature-wind-wrapper">
              <!-- <span class="material-icons">device_thermostat</span> -->
              <span class="city-info-temperature-wrapper"><span> {{ this.city.currentTemperature | number : '1.0-0'}}&#176;</span> </span>
              <span class="city-info-wind-wrapper">
              <span class="material-icons">air</span> {{ this.city.currentWind | number : '1.0-0'}}<span>km/h</span>
            </span>
            </div>
            <div class="row city-info-forecast-container">
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 city-info-forecast-titles-wrapper" >
                <div><p>TIME</p></div>
                <div><span class="material-icons">device_thermostat</span></div>
                <div><span class="material-icons">air</span></div>
              </div>
              <div *ngFor="let forecast of city.forecastForNextHours" class="col-lg-2 col-md-2 col-sm-2 col-xs-2" >
                <div class="city-info-forecast-time-wrapper">{{forecast.time | date:'shortTime'}}</div>
                <div class="city-info-forecast-temperature-wrapper">{{forecast.temperature | number : '1.0-0'}}&#176;</div>
                <div class="city-info-forecast-wind-wrapper">{{forecast.wind | number : '1.0-0'}}km/h</div>
              </div>

            </div>
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5 city-image-wrapper" [ngClass]="getTheImageForTheCity()">
          </div>
        </div>
      </div>
      <div *ngIf="showLoadingIndicator" class=" col-lg-12 city-details-loading-wrapper">
        <span class="material-icons">pending</span>
        <div>LOADING</div>
      </div>
      <div *ngIf="!city && !showLoadingIndicator" class=" col-lg-12 city-details-loading-wrapper">
        <span class="material-icons">error</span>
        <div>Data Loading Error</div>
      </div>
    </div>
  `,
  styleUrls: ['./city-details-component.css']
})
export class CityDetailsComponent implements OnInit{
  city:ICity;
  showLoadingIndicator:boolean;
  currentDayOfMonth:any;
  currentMonth:any;

  constructor(private cityService:CityService, private route:ActivatedRoute) {
    this.city = <ICity>{}
    this.showLoadingIndicator = false;
    this.currentDayOfMonth = formatDate(new Date(), 'EE MM', 'en')
    this.currentMonth = formatDate(new Date(), 'MMMM', 'en')
  }

  ngOnInit() {
    this.initData()
  }

  initData() {
    this.showLoadingIndicator = true;
    this.cityService.getCityInfo( +this.route.snapshot.params['id']).subscribe(city => {
        this.city = city;
        this.showLoadingIndicator = false;
      },
      error => {
        console.log(error);
        this.showLoadingIndicator = false;
      })
  }

  getTheImageForTheCity() {
    if(!this.city) return {};

    if(this.city.id === CityEnum.Amsterdam) return ['background-image-amsterdam-portrait'];
    if(this.city.id === CityEnum.Berlin) return ['background-image-berlin-portrait'];
    if(this.city.id === CityEnum.Rome) return ['background-image-rome-portrait'];
    if(this.city.id === CityEnum.Paris) return ['background-image-paris-portrait'];
    if(this.city.id === CityEnum.Milan) return ['background-image-milan-portrait'];
    return [];
  }
}
