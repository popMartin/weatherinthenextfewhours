import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import {  CityDetailsComponent } from './city-details.component';
import { CityService } from '../../../shared/services/index';
import { ICity } from '../../../shared/models/index';
import * as Rx from 'rxjs';
import { delay } from "rxjs/operators";

const expectedCityInfo: ICity =
  {
    id: 0,
    name: 'Amsterdam',
    currentTemperature: 12,
    currentWind: 14,
    forecastForNextHours: [
      {
        time: new Date(234256785),
        temperature: 23,
        wind: 12
      }
    ],
  }

describe('CityDetailsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        CityDetailsComponent
      ],
      providers : [
        CityService
      ]
    }).compileComponents();
  }));

  it('should create CityDetails component', () => {
    const fixture = TestBed.createComponent(CityDetailsComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit', () => {
    const fixture = TestBed.createComponent(CityDetailsComponent);
    const component = fixture.debugElement.componentInstance;
    let spy_initData = spyOn(component,"initData").and.returnValue([]);
    component.ngOnInit();
    expect(component.city).toEqual(<ICity>{});
  })

  it('should call initData and get object as response', fakeAsync(() => {
    const fixture = TestBed.createComponent(CityDetailsComponent);
    const component = fixture.debugElement.componentInstance;
    const service = fixture.debugElement.injector.get(CityService);
    let spy_city = spyOn(service,"getCityInfo").and.callFake(() => {
      return Rx.of(expectedCityInfo).pipe(delay(2000));
    });
    component.initData();
    tick(1000);
    expect(component.showLoadingIndicator).toEqual(true);
    tick(1000);
    expect(component.showLoadingIndicator).toEqual(false);
    expect(component.city).toEqual(expectedCityInfo);
  }))

  it('#getTheImageForTheCity returns the correct css class (string) that contains the image of the city', () => {
    const fixture = TestBed.createComponent(CityDetailsComponent);
    const component = fixture.debugElement.componentInstance;
    component.city = expectedCityInfo;
    const cityCssClass = component.getTheImageForTheCity();
    const cityName = expectedCityInfo.name && expectedCityInfo.name.toLowerCase()
    expect(cityCssClass[0]).toContain(cityName)
  });

});
