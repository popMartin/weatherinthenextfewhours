import {Component, Input} from '@angular/core'
import {formatDate} from '@angular/common';
import {CityEnum} from '../../../shared/enums';

@Component({
  selector: 'city-thumbnail',
  template: `
    <div class="thumbnail-wrapper" [routerLink]="['/cities', city.id]"  [ngClass]="getTheImageForTheCity()" >
      <div class="card">
       <div class="row card-top-row" >
         <div class="col-lg-12 card-city-name-wrapper">
           <div> {{city.name}} </div>
         </div>
       </div>
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6 card-date-time-wrapper">
              <p>{{currentTime}}</p>
              <p>{{currentDate }}</p>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 card-temperature-wind-wrapper">
              <h1> <span class="material-icons">device_thermostat</span><span>{{city.currentTemperature | number : '1.0-0'}}&#176;</span> </h1>
              <h4> <span class="material-icons">air</span><span> {{ city.currentWind | number : '1.0-0'}}km/h</span></h4>
            </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./city-thumbnail-component.css']
})

export class CityThumbnailComponent {
  @Input() city: any;
  currentDate:any;
  currentTime:any;

  constructor() {
    this.currentDate = formatDate(new Date(), 'EEEE, MM/YYYY', 'en');
    this.currentTime = formatDate(new Date(), 'hh:mm a', 'en');
    setInterval(() => {
      this.currentTime = formatDate(new Date(), 'hh:mm a', 'en');
    }, 5000);

  }


  getTheImageForTheCity() {
    if(!this.city) return {};

    if(this.city.id === CityEnum.Amsterdam) return ['background-image-amsterdam'];
    if(this.city.id === CityEnum.Berlin) return ['background-image-berlin'];
    if(this.city.id === CityEnum.Rome) return ['background-image-rome'];
    if(this.city.id === CityEnum.Paris) return ['background-image-paris'];
    if(this.city.id === CityEnum.Milan) return ['background-image-milan'];
    return [];
  }
}
