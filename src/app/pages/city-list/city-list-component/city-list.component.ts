import { Component, OnInit } from '@angular/core'
import { ICity } from 'src/app/shared/models/index'
import { ActivatedRoute } from '@angular/router'
import { CityService } from '../../../shared/services/index'
import { catchError } from 'rxjs/operators'


@Component({
  template: `
    <div class="city-list-wrapper">
      <div class="city-list-header">
        <h1> Europe Weather </h1>
      </div>

      <div class="row">
        <div *ngFor="let city of cities" class="col-md-4">
          <city-thumbnail [city]="city"></city-thumbnail>
        </div>
      </div>

      <div *ngIf="showLoadingIndicator" class=" col-lg-12 city-list-loading-wrapper">
        <span class="material-icons">pending</span>
        <div>LOADING</div>
      </div>
      <div *ngIf="(!cities || !cities.length) && !showLoadingIndicator" class=" col-lg-12 city-list-loading-wrapper">
        <span class="material-icons">error</span>
        <div>Data Loading Error</div>
      </div>
    </div>
  `,
  styleUrls: ['./city-list-component.css']
})

export class CityListComponent implements OnInit {
  cities:ICity[];
  showLoadingIndicator:boolean;

  constructor(private route:ActivatedRoute, private cityService:CityService) {
    this.cities = [];
    this.showLoadingIndicator = false;
  }

  ngOnInit() {
    this.initData()
  }

  initData() {
    this.showLoadingIndicator = true;
    this.cityService.getCitiesInfo().subscribe((cities: []) => {
        this.cities = cities;
        this.showLoadingIndicator = false;
      },
      error => {
        console.log(error);
        this.showLoadingIndicator = false;
      })
  }

}
