# Project overview

There are two pages in the app. The first page is for displaying the cities, their current temperature, and wind information. The second page is for displaying the forecast in the next few hours for the selected city.

## Folder structure

For maintaining clean, readable code and also for consistency, I have made the following decisions for the folder structure.
After using the Angular CLI to bootstrap the project, I have added three main directories (pages, router and shared) in which I have put all the files for the project. Furthermore, my shared directory contains enums, models, and services directories. For the CSS, I decided to use separate files in every component. Also, I organized all of my exports in barrels

## Project flow

Because it is a web app, I made sure that it was possible to visit both pages through links without problems. For navigating between pages, I used a router.
For obtaining the data from the API, I created service.
The service handles HTTP requests and provides functions for populating the data for the pages, or more precisely, for the components of the pages.
The five requests for the weather of the five European cities are sent from a single function using the forkJoin operator from the rxJS library. After the initial get request, the data is stored in a variable. Instead of sending additional requests while navigating the app, the data is returned from memory. I have decided to store the data because it is not updated rapidly. At this stage, I didn't pay much attention to the fact that it will remain the same data if the user is not reloading the page.
For further development of the app, my idea is that the data should update every hour. That can be done relatively easy because
the data already contains the date, so before returning it to the page it should be checked, and if one hour is passed, then a new request should be sent.

## The CSS and additional resources

I developed the CSS with the mobile-first approach. I installed Twitter bootstrap, but I used just the grid system for positioning, and the rest is pure CSS. I used rem units for better control of the text size when covering different screen sizes. Other resources that I am using are material design icons and Roboto font.

## Unit tests

I added unit tests for my service, all of my components, and the router.
